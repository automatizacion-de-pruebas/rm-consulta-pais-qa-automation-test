package pe.interbank.automatizacion.rm.data.services;

import com.aventstack.extentreports.Status;
import pe.interbank.automatizacion.rm.data.utilities.ExtendReportsUtilities;

public class TestStep extends HostServices {

	public String status = null;
	SharedServices sharedServices = new SharedServices();

	public TestStep() {
		super();
	}

	public void keyEnter() throws Throwable {
		this.waitForInputReady();
		this.sendEnter();
		this.waitForInputReady();
		Thread.sleep(500);
	}

	public void loginCONT() throws Throwable {
		this.waitForInputReady();
		this.sendKeysOn(20, 44, "sys");
		this.sendEnter();
		Thread.sleep(1000);
		this.waitForInputReady();
		this.sendClear();
		Thread.sleep(1000);
	}

	public void sendDelete(int x) {
		for (int i = 1; i < x; i++) {
			this.sendDELETE();
			this.waitForInputReady();
		}
	}

	public String CP_22(String cu, String pais, String rutaReporte, ExtendReportsUtilities report, int row) throws Throwable {

		String msg = "";
		this.waitForInputReady();
		this.sendKeysOn(5, 19, cu);
		this.keyEnter();
		report.getScreenShotWithState(Status.PASS, "Datos de Piases - Antes Modificacion", rutaReporte + "\\screenshots\\" + sharedServices.getHoraExacta() + ".png", null);
		this.sendKeysOn(16, 29, pais);
		this.keyEnter();

		msg = this.getText(22, 5, 15);
		if (msg.contentEquals("Datos Correctos")) {
			this.waitForInputReady();
			this.sendPF(5);
			this.waitForInputReady();
			Thread.sleep(500);
			report.getScreenShotWithState(Status.PASS, "Datos de Piases - Despues Modificacion", rutaReporte + "\\screenshots\\" + sharedServices.getHoraExacta() + ".png", null);
			status = "Pais Actualizado";
		} else {
			report.getScreenShotWithState(Status.PASS, "Datos de Piases - Despues Modificacion", rutaReporte + "\\screenshots\\" + sharedServices.getHoraExacta() + ".png", null);
			status = "Pais No Actualizado";
		}
		return status;
	}

	public void login_CP22(String appNum) throws Throwable {
		this.waitForInputReady();
		this.sendKeysOn(20, 44, appNum);
		keyEnter();
		this.sendKeysOn(19, 44, "1"); // Menu Principal
		keyEnter();
		Thread.sleep(500);
	}

}
