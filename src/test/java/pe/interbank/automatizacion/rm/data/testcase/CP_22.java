package pe.interbank.automatizacion.rm.data.testcase;

import com.aventstack.extentreports.Status;
import pe.interbank.automatizacion.rm.data.services.LoginHostServices;
import pe.interbank.automatizacion.rm.data.services.SharedServices;
import pe.interbank.automatizacion.rm.data.services.TestStep;
import pe.interbank.automatizacion.rm.data.utilities.Excel;
import pe.interbank.automatizacion.rm.data.utilities.ExtendReportsUtilities;
import org.testng.annotations.*;

import java.io.FileReader;
import java.text.ParseException;
import java.util.Properties;

public class CP_22 {

	private ExtendReportsUtilities report = new ExtendReportsUtilities();
	private LoginHostServices services = new LoginHostServices();
	private TestStep testStep = new TestStep();
	Properties properties = new Properties();
	private SharedServices sharedServices = new SharedServices();
	private String rutaReporte = this.sharedServices.getDirectorioProyecto() + "\\reportes\\" + this.sharedServices.getHoraExacta() + "-CP_22";
	// private Excel excel = new Excel();
	// private DataBase db = new DataBase();
	private boolean hostLogin = false;
	int row = 0;

	@BeforeClass
	public void beforeClass() {
		report.setUp(rutaReporte);
		report.test();
		report.crearDirectivo();
	}

	@DataProvider(name = "dp")
	public Object[][] dp() throws ParseException, Throwable {

		properties.load(new FileReader("config.properties"));
		String path = properties.getProperty("ruta");
		int rownum = Excel.getRowCount(path, "CP_22");
		int colcount = Excel.getCellCount(path, "CP_22", 0);

		String logindata[][] = new String[rownum][colcount];

		for (int i = 1; i <= rownum; i++) {
			for (int j = 0; j < colcount; j++) {
				logindata[i - 1][j] = Excel.getCellData(path, "CP_22", i, j);// 1
																				// 0
			}
		}
		return logindata;
	}

	@Test(dataProvider = "dp")
	public void testMain(String index, String cu, String pias, String status) throws Throwable {

		//System.out.println("Executing CP22 ********************************");
		row++;
		try {
			report.getTestObject().info("Inicio de Caso " + index);
			properties.load(new FileReader("config.properties"));

			if (!hostLogin) {
				this.services.login(properties.getProperty("user"), properties.getProperty("pass"), properties.getProperty("application"));
				this.testStep.login_CP22("13");// this number may change for
												// different user
				hostLogin = true;
			}

			if (hostLogin == true) {
				String estado = testStep.CP_22(cu, pias, rutaReporte, report, row);
				Excel.setCellData(properties.getProperty("ruta"), "CP_22", row, 3, estado);
			} else {
				Excel.setCellData(properties.getProperty("ruta"), "CP_22", row, 3, "FAIL - Host Logon Error");
			}
		} catch (Exception e) {
			e.printStackTrace();
			report.getScreenShotWithState(Status.ERROR, e.getMessage(), rutaReporte + "\\screenshots\\" + this.sharedServices.getHoraExacta() + ".png", null);
			Excel.setCellData(properties.getProperty("ruta"), "CP_22", row, 3, e.getMessage());
		}
		report.getTestObject().info("Fin de Caso");
	}

	@AfterMethod
	public void afterTest() throws Throwable {
		this.services.sendPF(3);
		this.services.waitForInputReady();
		Thread.sleep(500);
		this.services.sendKeysOn(19, 44, "1");
		this.testStep.keyEnter();
	}

	@AfterClass
	public void afterClass() throws Throwable {
		int n = 0;
		while (n < 3) {
			this.services.sendPF(3);
			this.services.waitForInputReady();
			n++;
		}
		this.services.logOff(hostLogin);
		report.getTestObject().info("Automatizacion Completa");
		report.terminarReporte(rutaReporte + "\\report.html");
	}

}
