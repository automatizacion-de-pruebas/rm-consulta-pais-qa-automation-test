package pe.interbank.automatizacion.rm.data.services;

import java.io.FileReader;
import java.util.Properties;

public class LoginHostServices extends HostServices {

	public LoginHostServices() {
		super();
	}

	public void login(String user, String pass, String app) throws Throwable {

		Properties properties = new Properties();
		properties.load(new FileReader("config.properties"));
		this.focusWindow("Sesión A - [24 x 80]");
		// Thread.sleep(30000);
		this.waitForInputReady();
		this.waitForCursor(6, 1);
		this.sendClear();
		this.sendKeys("logon applid(access30)");
		this.sendEnter();
		this.waitForInputReady();
		this.waitForText(2, 2, 6, "ACCESS");
		Thread.sleep(1000);
		this.waitForInputReady();
		this.sendKeysOn(10, 68, user);
		this.sendKeysOn(11, 68, pass);
		this.waitForInputReady();
		this.sendEnter();
		this.waitForText(1, 2, 6, "EMSP01");
		this.sendKeys("lf " + properties.getProperty("application"));
		this.sendEnter();
		this.waitForInputReady();
		Thread.sleep(1000);
		this.sendKeys(properties.getProperty("application"));
		this.sendEnter();
		this.waitForInputReady();
		this.waitForText(20, 59, 9, "CONSULTA");
		this.sendKeys("ib00");
		this.waitForInputReady();
		this.sendEnter();
		this.waitForInputReady();
		Thread.sleep(1000);
		String mensaje = this.getText(1, 2, 4);

		if (mensaje.contains("CICS")) {

			this.waitForInputReady();
			this.waitForCursor(10, 71);
			this.sendKeysOn(10, 71, user);
			this.sendKeysOn(11, 71, pass);
			this.sendEnter();
			this.waitForInputReady();
			Thread.sleep(1000);
		}

	}

	public void logOffMenu() throws InterruptedException {

		this.waitForInputReady();
		String titulo = this.getText(1, 28, 25);
		int a = 0;

		while (!titulo.contains("SISTEMAS DE INFORMACION")) {
			//System.out.println(titulo);
			this.sendPF(3);
			Thread.sleep(500);
			titulo = this.getText(1, 28, 25);
			a++;
			if (a == 10) {
				break;
			}
		}
	}

	public void logOff(boolean login) throws Throwable {
		// logOffMenu();
		//System.out.println("Entro a logoff");
		this.sendClear();
		this.waitForInputReady();
		this.sendPF(3);
		this.waitForInputReady();

		if (login) {
			this.waitForText(1, 24, 25, "Seleccion de Aplicaciones");
		}
		this.sendPF(12);
		this.waitForInputReady();
		this.waitForText(1, 19, 5, "LOGON");
	}
}
